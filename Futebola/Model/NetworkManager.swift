//
//  NetworkManager.swift
//  Futebola
//
//  Created by Gilson Gil on 6/6/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation
import Alamofire
import AEXML

extension Request {
  class func imageResponseSerializer() -> Serializer {
    return { request, response, data in
      if data == nil {
        return (nil, nil)
      }
      
      let image = UIImage(data: data!, scale: UIScreen.mainScreen().scale)
      
      return (image, nil)
    }
  }
  
  func responseImage(completionHandler: (NSURLRequest, NSHTTPURLResponse?, UIImage?, NSError?) -> Void) -> Self {
    return response(serializer: Request.imageResponseSerializer(), completionHandler: { (request, response, image, error) in
      completionHandler(request, response, image as? UIImage, error)
    })
  }
}

struct NetworkManager {
  enum Router: URLRequestConvertible {
    static let baseURLString = "http://www.futvivo.com.br/doisdoissete/"
    static var authToken: String?
    
    case Standings(Int)
    
    var path: String {
      switch self {
      case .Standings:
        //http://www.futvivo.com.br/doisdoissete/listaclassificacao.php?idcampeonato=1109
        return "listaclassificacao.php"
      }
    }
    
    var URLRequest: NSURLRequest {
      let URL = NSURL(string: Router.baseURLString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)!
      let mutableURLRequest = NSMutableURLRequest(URL: URL.URLByAppendingPathComponent(path))
      mutableURLRequest.HTTPMethod = "GET"
      
      switch self {
      case .Standings(let id):
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: ["idcampeonato": id]).0
      default:
        return ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0
      }
    }
  }
}

// MARK: Parse
extension NetworkManager {
  static func parse(router: Router, xmlDoc: AEXMLDocument, completion: Result<AnyObject> -> ()) {
    switch router {
    case .Standings:
      let root = xmlDoc.root["Classificacoes"]["Classificacao"]
      var standings = [Standing]()
      for standingAPI in root.children {
        let standing = Standing(standingAPI)
        standings.append(standing)
      }
      completion(Result(standings))
    }
  }
}

// MARK: API
extension NetworkManager {
  static func request(router: Router, completion: Result<AnyObject> -> ()) {
    Alamofire.Manager.sharedInstance.request(router)
    .response { _, _, data, error in
      println(NSString(data: data as! NSData, encoding: NSUTF8StringEncoding))
      var error: NSError?
      if let data = data as? NSData, let xmlDoc = AEXMLDocument(xmlData: data, error: &error) {
        self.parse(router, xmlDoc: xmlDoc, completion: completion)
      } else {
        println("description: \(error?.localizedDescription)\ninfo: \(error?.userInfo)")
      }
      dispatch_async(dispatch_get_main_queue()) {
        
      }
    }
  }
}
