//
//  Club.swift
//  Futebola
//
//  Created by Gilson Gil on 6/6/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class Club {
  let id: Int
  let name: String
  let shortName: String
  
  init(id: Int, name: String, shortName: String) {
    self.id = id
    self.name = name
    self.shortName = shortName
  }
}
