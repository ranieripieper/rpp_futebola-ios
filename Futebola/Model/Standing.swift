//
//  Standing.swift
//  Futebola
//
//  Created by Gilson Gil on 6/6/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation
import AEXML

class Standing {
  let club: Club
  var position: Int
  var points: Int
  var played: Int
  var won: Int
  var drawn: Int
  var lost: Int
  var goalsFor: Int
  var goalsAgainst: Int
  var goalDifference: Int
  var homePoints: Int
  var homePlayed: Int
  var homeWon: Int
  var homeDrawn: Int
  var homeLost: Int
  var homeGoalsFor: Int
  var homeGoalsAgainst: Int
  var homeGoalDifference: Int
  var awayPoints: Int
  var awayPlayed: Int
  var awayWon: Int
  var awayDrawn: Int
  var awayLost: Int
  var awayGoalsFor: Int
  var awayGoalsAgainst: Int
  var awayGoalDifference: Int
  var performance: String
  
  init(_ element: AEXMLElement) {
    club = Club(id: (element.attributes["Id"] as! String).toInt() ?? 0, name: element.attributes["Nome"] as! String, shortName: element.attributes["Sigla_Nome"] as! String)
    position = 0
    points = 0
    played = 0
    won = 0
    drawn = 0
    lost = 0
    goalsFor = 0
    goalsAgainst = 0
    goalDifference = 0
    homePoints = 0
    homePlayed = 0
    homeWon = 0
    homeDrawn = 0
    homeLost = 0
    homeGoalsFor = 0
    homeGoalsAgainst = 0
    homeGoalDifference = 0
    awayPoints = 0
    awayPlayed = 0
    awayWon = 0
    awayDrawn = 0
    awayLost = 0
    awayGoalsFor = 0
    awayGoalsAgainst = 0
    awayGoalDifference = 0
    performance = ""
    for child in element.children {
      switch child.name {
      case "Posicao":
        position = child.intValue
      case "Pontos_Ganhos":
        points = child.intValue
      case "Jogos":
        played = child.intValue
      case "Vitorias":
        won = child.intValue
      case "Empates":
        drawn = child.intValue
      case "Dettoras":
        lost = child.intValue
      case "Gols_Pro":
        goalsFor = child.intValue
      case "Gols_Contra":
        goalsAgainst = child.intValue
      case "Saldo_Gols":
        goalDifference = child.intValue
      case "Pontos_Ganhos_Casa":
        homePoints = child.intValue
      case "Jogos_Casa":
        homePlayed = child.intValue
      case "Vitorias_Casa":
        homeWon = child.intValue
      case "Empates_Casa":
        homeDrawn = child.intValue
      case "Derrotas_Casa":
        homeLost = child.intValue
      case "Gols_Pro_Casa":
        homeGoalsFor = child.intValue
      case "Gols_Contra_Casa":
        homeGoalsAgainst = child.intValue
      case "Saldo_Gols_Casa":
        homeGoalDifference = child.intValue
      case "Pontos_Ganhos_Fora":
        awayPoints = child.intValue
      case "Jogos_Fora":
        awayPlayed = child.intValue
      case "Vitorias_Fora":
        awayWon = child.intValue
      case "Empates_Fora":
        awayDrawn = child.intValue
      case "Derrotas_Fora":
        awayLost = child.intValue
      case "Gols_Pro_Fora":
        awayGoalsFor = child.intValue
      case "Gols_Contra_Fora":
        awayGoalsAgainst = child.intValue
      case "Saldo_Gols_Fora":
        awayGoalDifference = child.intValue
      case "Aproveitamento":
        performance = child.value ?? "0%"
      default:
        break
      }
    }
  }
}
