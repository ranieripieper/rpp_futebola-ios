//
//  HomeViewController.swift
//  Futebola
//
//  Created by Gilson Gil on 6/6/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import UIKit
import Alamofire

class HomeViewController: UIViewController {
  @IBOutlet weak var standingsCollectionDelegate: StandingsCollectionDelegate!
  @IBOutlet weak var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    NetworkManager.request(.Standings(1109)) {
      switch $0 {
      case .Success(let boxed):
        if let standings = boxed.unbox as? [Standing] {
          self.standingsCollectionDelegate.datasource = standings
          self.tableView.reloadData()
        }
      case .Failure(let error):
        println(error.description)
      }
    }
  }
}
