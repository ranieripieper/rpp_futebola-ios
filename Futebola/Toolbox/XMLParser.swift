//
//  XMLParser.swift
//  Futebola
//
//  Created by Gilson Gil on 6/6/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

class XMLParser: NSObject {
  var currentElementValue: String = ""
  var dictionary: [String: AnyObject]?
  var currentKey: String?
}

extension XMLParser: NSXMLParserDelegate {
  func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [NSObject : AnyObject]) {
    self.currentElementValue = ""
  }
  
  func parser(parser: NSXMLParser, foundCharacters string: String?) {
    
  }
}