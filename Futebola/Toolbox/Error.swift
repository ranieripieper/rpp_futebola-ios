//
//  Error.swift
//  Networking
//
//  Created by Gilson Gil on 2/13/15.
//  Copyright (c) 2015 doisdoissete. All rights reserved.
//

import Foundation

struct Error {
  typealias ErrorInfoDictionary = Dictionary<String, Any>
  
  let code: Int
  let domain: String
  let description: String?
  
  init(code: Int, domain: String, description: String?) {
    self.code = code
    self.domain = domain
    self.description = description
  }
}